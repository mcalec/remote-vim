" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
" do not keep a backup file, use versions instead
  set nobackup
else
" keep a backup file
set backup
endif
set backupdir=~/.vim/backups

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  " I think that sucks.  Not sure where I got it from.
  " Let's comment it out.
  " autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")


execute pathogen#infect()

syntax on

set background=dark
let g:solarized_termcolors=256
set term=screen-256color
colorscheme solarized

filetype plugin indent on

" Indentation
set expandtab
set shiftwidth=2
set tabstop=2
set softtabstop=2
set autoindent

set relativenumber
set number

set smartcase

" Use Ctrl+s to save
" note this requires shell configuration/compatibility,
" sometimes with terminal emulator, in my case in zshrc.
" works for 'insert' mode
inoremap <c-s> <Esc>:w<CR>
" works for 'normal' mode
nnoremap <c-s> :w<CR>

" line-wrapping
set wrap linebreak
set breakindent
let &showbreak='  '

" For Splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set splitbelow
set splitright

" For navigating in and out of TMUX
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
nnoremap <silent> {Previous-Mapping} :TmuxNavigatePrevious<cr>

" Set cursor shape to reflect insert/normal mode
let &t_SI = "\<Esc>[6 q"
let &t_EI = "\<Esc>[2 q"
